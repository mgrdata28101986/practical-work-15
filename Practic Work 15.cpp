﻿// Практическая работа #15
#include <iostream>
using std::cout;
using std::endl;

int CheckNumberParity ( int number , int isOdd )
{
    if (isOdd) return number % 2 ? true : false;

    return number % 2 ? false : true;    
}

int main()
{
    const int n { 100 };
        
    for (int i {0}; i < n; i++)
    {
        if ( CheckNumberParity ( i , false ) )  cout << i << endl;
    }

    return 0;
}


